class Temperature
  def initialize(temp = {})
    @temp = temp
  end

  def in_fahrenheit
    if @temp.include?(:f)
      @temp[:f]
    else
      convert_c_to_f(@temp[:c])
    end
  end

  def in_celsius
    if @temp.include?(:c)
      @temp[:c]
    else
      convert_f_to_c(@temp[:f])
    end
  end
  
  def convert_f_to_c(degrees)
    (degrees - 32) * 5 / 9.0
  end
  def convert_c_to_f(degrees)
    degrees * 9.0 / 5 + 32
  end

  def self.from_celsius(degrees)
    Temperature.new(:c => degrees)
  end

  def self.from_fahrenheit(degrees)
    Temperature.new(:f => degrees)
  end
end

class Celsius < Temperature
  def initialize(degrees)
    @temp = {:c => degrees}
  end
end

class Fahrenheit < Temperature
  def initialize(degrees)
    @temp = {:f => degrees}
  end
end
