class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_string
    arr = []
    if @seconds >= 3600
      hours = @seconds / 60 / 60
      min = (@seconds - hours * 3600) / 60
      sec = @seconds - (hours * 3600) - (min * 60)
    elsif @seconds >= 60
      hours = 0
      min = @seconds / 60
      sec = @seconds - (min * 60)
    else
      hours = 0
      min = 0
      sec = @seconds
    end
    arr << hours << min << sec
    arr.map {|num| padded(num)}.join(":")
  end

  def padded(num)
    str = num.to_s
    if str.length == 2
      str
    else
      "0" + str
    end
  end
end
