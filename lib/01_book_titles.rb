class Book
  attr_accessor :title

  def title
    @title = @title.split(" ")
    @title.map!.with_index do |word, idx|
      if idx == 0
        word.capitalize
      elsif is_exception?(word)
        word
      else
        word.capitalize
      end
    end
    @title = @title.join(" ")
  end

  def is_exception?(word)
    ["the", "a", "an", "and", "in", "of"].include?(word)
  end
end
