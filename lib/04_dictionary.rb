class Dictionary
  def initialize(dict = {})
    @dict = dict
  end

  def add(entry)
    if entry.is_a? String
      @dict[entry] = nil
    else
      @dict[entry.keys.first] = entry.values.first
    end
  end

  def entries
    @dict
  end

  def keywords
    @dict.keys.sort
  end

  def include?(word)
    @dict.include?(word)
  end

  def find(word)
    @dict.select {|k, v| k.include?(word)}.to_h
  end

  def printable
    str = ""
    sorted_hash = @dict.sort_by {|k, v| k}
    sorted_hash.each.with_index do |arr, i|
      str += "[#{arr.first}] "
      str += "\"#{arr.last}\""
      str += "\n" unless i == sorted_hash.length - 1
    end
    print str
    str
  end
end
